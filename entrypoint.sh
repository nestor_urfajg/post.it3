#!/bin/bash

set -e

sed -i 's/SITENAME/'"$SITENAME"'/' /var/www/html/index.html

exec "$@"
